import React, { useState } from 'react';
import {
    Breadcrumb, BreadcrumbItem,
    Button, Col, Label,
    Form, FormGroup, Input
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

// const required = (val) => val && val.length;
// const maxLength = (len) => (val) => !(val) || (val.length <= len);
// const minLength = (len) => (val) => val && (val.length >= len);
// const isNumber = (val) => !isNaN(Number(val));
// const validEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);

const schema = yup.object().shape({
    firstname: yup.string().min(2).required(),
    lastname: yup.string().min(2).required(),
    email: yup.string().email(),
    feedback: yup.string().min(5).required()
});

const Contact = () => {
    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(schema)
    });
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [telNum, setTelNum] = useState('');
    const [agree, setAgree] = useState(false);
    const [contactType, setContactType] = useState('Tel.');
    const [message, setMessage] = useState('');

    const onSubmit = () => {
        let feedback = {
            firstName,
            lastName,
            email,
            telNum,
            agree,
            contactType,
            message
        };
        console.log(feedback);
        alert(`Thank you for your feedback!\n${JSON.stringify(feedback)}`);
        resetForm();
    };

    // const onSubmit = data => {
    //     console.log(data);
    // };

    const resetForm = () => {
        setFirstName('');
        setLastName('');
        setEmail('');
        setTelNum('');
        setAgree(false);
        setContactType('Tel.');
        setMessage('');
    }

    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Contact Us</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Contact Us</h3>
                    <hr />
                </div>
            </div>
            <div className="row row-content">
                <div className="col-12">
                    <h3>Location Information</h3>
                </div>
                <div className="col-12 col-sm-4 offset-sm-1">
                    <h5>Our Address</h5>
                    <address>
                        121, Clear Water Bay Road<br />
                            Clear Water Bay, Kowloon<br />
                            HONG KONG<br />
                        <i className="fa fa-phone"></i>: +852 1234 5678<br />
                        <i className="fa fa-fax"></i>: +852 8765 4321<br />
                        <i className="fa fa-envelope"></i>: <a href="mailto:confusion@food.net">confusion@food.net</a>
                    </address>
                </div>
                <div className="col-12 col-sm-6 offset-sm-1">
                    <h5>Map of our Location</h5>
                </div>
                <div className="col-12 col-sm-11 offset-sm-1">
                    <div className="btn-group" role="group">
                        <a role="button" className="btn btn-primary" href="tel:+85212345678"><i className="fa fa-phone"></i> Call</a>
                        <a role="button" className="btn btn-info" href="/contactus"><i className="fa fa-skype"></i> Skype</a>
                        <a role="button" className="btn btn-success" href="mailto:confusion@food.net"><i className="fa fa-envelope-o"></i> Email</a>
                    </div>
                </div>
            </div>
            <div className="row row-content">
                <div className="col-12">
                    <h3>Send us your Feedback</h3>
                </div>
                <div className="col-12 col-md-9">
                    <Form onSubmit={handleSubmit(onSubmit)}>
                        <FormGroup row>
                            <Label htmlFor="firstname" md={2}>First Name</Label>
                            <Col md={10}>
                                <input
                                    ref={register}
                                    type="text"
                                    name="firstname"
                                    id="firstname"
                                    placeholder="First Name"
                                    className="form-control"
                                    value={firstName || ''}
                                    onChange={(event) => {
                                        setFirstName(event.target.value);
                                    }}
                                />
                                <p className="text-danger">{errors.firstname?.message}</p>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label htmlFor="lastname" md={2}>Last Name</Label>
                            <Col md={10}>
                                <input
                                    ref={register}
                                    type="text"
                                    name="lastname"
                                    id="lastname"
                                    placeholder="Last Name"
                                    className="form-control"
                                    value={lastName || ''}
                                    onChange={(event) => {
                                        setLastName(event.target.value);
                                    }}
                                />
                                <p className="text-danger">{errors.lastname?.message}</p>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label htmlFor="telnum" md={2}>Contact Tel.</Label>
                            <Col md={10}>
                                <input
                                    ref={register}
                                    type="text"
                                    name="telnum"
                                    id="telnum"
                                    placeholder="Tel. Num"
                                    className="form-control"
                                    value={telNum || ''}
                                    onChange={(event) => {
                                        setTelNum(event.target.value);
                                    }}
                                />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label htmlFor="email" md={2}>Email</Label>
                            <Col md={10}>
                                <input
                                    ref={register}
                                    type="text"
                                    name="email"
                                    id="email"
                                    placeholder="Email"
                                    className="form-control"
                                    value={email || ''}
                                    onChange={(event) => {
                                        setEmail(event.target.value);
                                    }}
                                />
                                <p className="text-danger">{errors.email?.message}</p>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col md={{ size: 6, offset: 2 }}>
                                <div className="form-check">
                                    <Label check>
                                        <Input
                                            ref={register}
                                            type="checkbox"
                                            name="agree"
                                            className="form-check-input"
                                            value={agree || false}
                                            checked={agree || false}
                                            onChange={(event) => {
                                                setAgree(!!event.target.value);
                                            }}
                                            id="agree" />{' '}
                                        <strong>May we contact you?</strong>
                                    </Label>
                                </div>
                            </Col>
                            <Col md={{ size: 3, offset: 1 }}>
                                <Input
                                    ref={register}
                                    type="select"
                                    name="contactType"
                                    className="form-control"
                                    value={contactType || 'Tel.'}
                                    onChange={(event) => {
                                        setContactType(event.target.value);
                                    }}
                                >
                                    <option>Tel.</option>
                                    <option>Email</option>
                                </Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label htmlFor="feedback" md={2}>Your Feedback</Label>
                            <Col md={10}>
                                <textarea
                                    ref={register}
                                    rows="12"
                                    className="form-control"
                                    id="feedback"
                                    name="feedback"
                                    value={message || ''}
                                    onChange={(event) => {
                                        setMessage(event.target.value);
                                    }}
                                />
                                <p className="text-danger">{errors.feedback?.message}</p>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col md={{ size: 10, offset: 2 }}>
                                <Button type="submit" color="primary">
                                    Send Feedback
                                </Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        </div>
    );
};

export default Contact;