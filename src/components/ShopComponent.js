import React from 'react';
import { Row, Col } from 'reactstrap';
import { Fade, Stagger } from 'react-animation-components';
import shopImages from '../shared/shopImages';
import QuantityInput from '../shared/QuantityInput';

const Shop = () => {
    const Item = ({ image }) => {


        return (
            <Col xs="12" sm="4" key={image.id}>
                <img className="img-fluid m-1" src={image.source} alt={image.description} />
                <Row className="justify-content-center text-center add-to-cart">
                    <Col xs="6">
                        <a href="/shop">Add to Cart</a>
                        <QuantityInput />
                    </Col>
                </Row>
            </Col>
        );
    };

    const ItemRow = ({ rowId }) => {
        return (
            <Stagger in={true}>
                <Fade in={true}>
                    <Row className="row-content justify-content-center">
                        {
                            shopImages.filter(image => image.rowId === rowId).map(image => {
                                return <Item image={image} key={image.id} />
                            })
                        }
                    </Row>
                </Fade>
            </Stagger>
        );
    };

    return (
        <div className="container">
            <ItemRow rowId={0} />
            <ItemRow rowId={1} />
            <ItemRow rowId={2} />
            <Row className="my-5 text-center">
                <Col xs="12">
                    <h2>More merchandise coming soon!</h2>
                </Col>
            </Row>
        </div>
    );
};

export default Shop;