import React from 'react';
import { Row, Col } from 'reactstrap';
import homeImages from '../shared/homeImages';
import { Fade, Stagger } from 'react-animation-components';

const ImageRow = ({ rowId }) => {
    return (
        <Stagger in={true}>
            <Fade in={true}>
                <Row className="row-content">

                    {
                        homeImages.filter(image => image.rowId === rowId).map(image => {
                            return (
                                <Col sm={4} xs={12} key={image.id}>
                                    <img className="img-fluid" src={image.source} alt={image.description} />
                                </Col>
                            );
                        })
                    }
                </Row>
            </Fade>
        </Stagger>
    )
}
const Home = () => {
    return (
        <div className="container">
            <ImageRow rowId={0} />
            <ImageRow rowId={1} />
            <ImageRow rowId={2} />
            <ImageRow rowId={3} />
            <div className="my-5 row text-center">
                <div className="col-12">
                    <h2>More art coming soon!</h2>
                </div>
            </div>
        </div>
    );
};

export default Home;