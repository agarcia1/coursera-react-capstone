import React from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';

const AboutMe = () => {
    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>About Me</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>About Me</h3>
                    <hr />
                </div>
            </div>
            <div className="row row-content">
                <div className="col-12 col-md-6">
                    <img className="img-fluid float-left pr-3 pb-3" src="images/pexels-ketut-subiyanto-4429568.jpg" alt="self" />
                </div>
                <div className="col-12 col-md-6">
                    <p>
                        Hi! My name is Jane Doe. I've been an artist pretty much all of my life. I focus mostly on oil paintings but I have done a few sculptures in my time.
                        <br /><br />
                        Art is such a wonderful way to release stress and express yourself. Especially now during this time of a global pandemic, it is very helpful when dealing with anxiety.
                        <br /><br />
                        This website is a way for me to post my artwork to the world, as well as let people purchase some of my merchandise. Even if you do not buy anything, I hope all of my creations have brightened up your day.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default AboutMe;