import React, { useState } from 'react';
import {
    Nav, NavItem, NavLink, Row,
    TabContent, TabPane, Col
} from 'reactstrap';
import classnames from 'classnames';
import { Fade, Stagger } from 'react-animation-components';
import artworkImages from '../shared/artworkImages';
import { imageTypes } from '../shared/imageTypes';

const Artwork = () => {
    const [activeTab, setActiveTab] = useState(imageTypes.abstractPainting);

    const toggle = tab => {
        if (activeTab !== tab) setActiveTab(tab);
    }

    const ImageRow = ({ rowId }) => {
        return (
            <Stagger in={true}>
                <Fade in={true}>
                    <Row>
                        {
                            artworkImages.filter(image => image.description === activeTab && image.rowId === rowId).map(image => {
                                console.log(image);
                                return (
                                    <Col xs="12" sm="4" key={image.id}>
                                        <img className="img-fluid m-1" src={image.source} alt={image.description} />
                                    </Col>
                                );
                            })
                        }
                    </Row>
                </Fade>
            </Stagger>
        );
    };

    return (
        <div className="container">
            <Row className="row-content justify-content-center">
                <Nav pills>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: activeTab === imageTypes.abstractPainting })}
                            onClick={() => { toggle(imageTypes.abstractPainting); }} >
                            Paintings
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: activeTab === imageTypes.photograph })}
                            onClick={() => { toggle(imageTypes.photograph); }} >
                            Photographs
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: activeTab === imageTypes.pottery })}
                            onClick={() => { toggle(imageTypes.pottery); }} >
                            Pottery
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink disabled>More coming soon!</NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={activeTab}>
                    <TabPane tabId={imageTypes.abstractPainting}>
                        <ImageRow rowId={0} />
                        <ImageRow rowId={1} />
                        <ImageRow rowId={2} />
                    </TabPane>
                    <TabPane tabId={imageTypes.photograph}>
                        <ImageRow rowId={0} />
                        <ImageRow rowId={1} />
                        <ImageRow rowId={2} />
                    </TabPane>
                    <TabPane tabId={imageTypes.pottery}>
                        <ImageRow rowId={0} />
                        <ImageRow rowId={1} />
                        <ImageRow rowId={2} />
                    </TabPane>
                </TabContent>
            </Row>
        </div>
    );
};

export default Artwork;