import React, { useState } from 'react';
import {
    Navbar, NavbarBrand, Nav, NavbarToggler,
    Collapse, NavItem, Jumbotron,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';

const Header = () => {
    const [isNavOpen, setIsNavOpen] = useState(false);
    const toggleNav = () => setIsNavOpen(!isNavOpen);

    return (
        <div>
            <Navbar className="fixed-top" dark expand="md">
                <div className="container">
                    <NavbarToggler onClick={toggleNav} />
                    <NavbarBrand className="mr-auto" href="/home"><img src='images/logo.png' height="40" width="40" alt='Jane Doe Artwork' /></NavbarBrand>
                    <Collapse isOpen={isNavOpen} navbar>
                        <Nav navbar>
                            <NavItem>
                                <NavLink className="nav-link" to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/artwork'><span className="fa fa-paint-brush fa-lg"></span> Artwork</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/shop'><span className="fa fa-shopping-cart fa-lg"></span> Shop</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/aboutme'><span className="fa fa-info fa-lg"></span> About Me</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/contact'><span className="fa fa-address-card fa-lg"></span> Contact</NavLink>
                            </NavItem>
                        </Nav>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink className="nav-link" to='/cart'><span className="fa fa-shopping-basket fa-lg"></span> Cart</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </div>
            </Navbar>
            <Jumbotron fluid={true}>
                <div className="container">
                    <div className="row row-header align-items-center">
                        <div className="col-12 col-sm-6">
                            <h1>Jane Doe</h1>
                            <p>Artwork made with love <i className="fa fa-heart" aria-hidden="true"></i> and inspired by anything I see around me, whether it's abstract or a landscape.</p>
                        </div>
                        <div className="col-12 col-sm-6 text-center">
                            <img src="images/logo.png" className="img-fluid" alt="Jane Doe Artwork" />
                        </div>
                    </div>
                </div>
            </Jumbotron>
        </div>
    );
}

export default Header;