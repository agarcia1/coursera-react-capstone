import React from 'react';
import cartImages from '../shared/cartImages';
import { Row, Col, Button } from 'reactstrap';
import QuantityInput from '../shared/QuantityInput';

const Cart = () => {
    const CartItemRow = ({ image }) => {
        return (
            <Row className="row-content">
                <Col xs="12" sm="6">
                    <img className="img-fluid" src={image.source} alt={image.description} />
                </Col>
                <Col xs="12" sm="6" className="add-to-cart justify-content-center text-center">
                    <p>
                        Custom-made T-shirt<br />
                        Price: <strong>$20</strong><br />
                        Sizes: XS, S, M, L, XL, XXL, XXXL
                    </p>
                    <QuantityInput />
                </Col>
            </Row>
        );
    };

    return (
        <div className="container">
            {
                cartImages.map(image => {
                    return (
                        <CartItemRow key={image.id} image={image} />
                    )
                })
            }
            <Row className="my-5 text-center">
                <Col xs="12">
                    <Button size="lg" color="success" block>Checkout</Button>
                </Col>
            </Row>
        </div>
    );
};

export default Cart;