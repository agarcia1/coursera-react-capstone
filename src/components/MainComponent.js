import React from 'react';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Artwork from './ArtworkComponent';
import Shop from './ShopComponent';
import AboutMe from './AboutMeComponent';
import Contact from './ContactComponent';
import Cart from './CartComponent';
import Home from './HomeComponent';
import { Switch, Route, Redirect } from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group';

const Main = () => {
    return (
        <div>
            <Header />
            <TransitionGroup>
                <CSSTransition classNames="page" timeout={300}>
                    <Switch>
                        <Route exact path='/home' component={() => <Home />} />
                        <Route exact path='/artwork' component={() => <Artwork />} />
                        <Route exact path='/shop' component={() => <Shop />} />
                        <Route exact path='/aboutme' component={() => <AboutMe />} />
                        <Route exact path='/contact' component={() => <Contact />} />
                        <Route exact path='/cart' component={() => <Cart />} />
                        <Redirect to="/home" />
                    </Switch>
                </CSSTransition>
            </TransitionGroup>
            <Footer />
        </div>
    );
};

export default Main;