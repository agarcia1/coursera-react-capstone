import { imageTypes } from './imageTypes';

const homeImages = [
    {
        rowId: 0,
        id: 0,
        source: 'images/pexels-sam-lion-5709641.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 0,
        id: 1,
        source: 'images/pexels-steve-johnson-1787242.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 0,
        id: 2,
        source: 'images/pexels-luizclas-556665.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 1,
        id: 3,
        source: 'images/pexels-zaksheuskaya-1561020.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 1,
        id: 4,
        source: 'images/pexels-leo-cardelli-1236701.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 1,
        id: 5,
        source: 'images/pexels-free-creative-stuff-1193743.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 2,
        id: 6,
        source: 'images/pexels-steve-johnson-1856455.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 2,
        id: 7,
        source: 'images/pexels-rahul-695644.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 2,
        id: 8,
        source: 'images/pexels-steve-johnson-1985682.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 3,
        id: 9,
        source: 'images/pexels-stephan-müller-1473215.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 3,
        id: 10,
        source: 'images/pexels-steve-johnson-1856456.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 3,
        id: 11,
        source: 'images/pexels-hristo-fidanov-1252869.jpg',
        description: imageTypes.photograph
    }
];

export default homeImages;