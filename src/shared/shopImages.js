import { imageTypes } from './imageTypes';

const shopImages = [
    {
        rowId: 0,
        id: 0,
        source: 'images/pexels-gabby-k-5273557.jpg',
        description: imageTypes.mug
    },
    {
        rowId: 0,
        id: 1,
        source: 'images/pexels-godisable-jacob-2116189.jpg',
        description: imageTypes.hoodie
    },
    {
        rowId: 0,
        id: 2,
        source: 'images/pexels-kaboompics-com-6272.jpg',
        description: imageTypes.coaster
    },
    {
        rowId: 1,
        id: 3,
        source: 'images/pexels-kristina-paukshtite-701771.jpg',
        description: imageTypes.shirt
    },
    {
        rowId: 1,
        id: 4,
        source: 'images/pexels-spencer-selover-428338.jpg',
        description: imageTypes.shirt
    },
    {
        rowId: 1,
        id: 5,
        source: 'images/pexels-spencer-selover-428340.jpg',
        description: imageTypes.shirt
    },
    {
        rowId: 2,
        id: 6,
        source: 'images/pexels-gabby-k-5063577.jpg',
        description: imageTypes.pottery
    },
    {
        rowId: 2,
        id: 7,
        source: 'images/pexels-sunsetoned-5913195.jpg',
        description: imageTypes.pottery
    },
    {
        rowId: 2,
        id: 8,
        source: 'images/pexels-karolina-grabowska-4202938.jpg',
        description: imageTypes.pottery
    }
];

export default shopImages;