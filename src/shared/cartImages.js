import { imageTypes } from './imageTypes';

const cartImages = [
    {
        id: 0,
        source: 'images/pexels-kristina-paukshtite-701771.jpg',
        description: imageTypes.shirt
    },
    {
        id: 1,
        source: 'images/pexels-spencer-selover-428338.jpg',
        description: imageTypes.shirt
    },
    {
        id: 2,
        source: 'images/pexels-spencer-selover-428340.jpg',
        description: imageTypes.shirt
    }
];

export default cartImages;