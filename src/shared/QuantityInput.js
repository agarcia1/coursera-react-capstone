import React, { useState } from 'react';
import { InputGroup, Input, Button, InputGroupAddon } from 'reactstrap';

const QuantityInput = () => {
    const [itemCount, setItemCount] = useState(0);
    const add = () => {
        setItemCount(itemCount + 1);
    };
    const subtract = () => {
        if (itemCount > 0) {
            setItemCount(itemCount - 1);
        }
    }

    return (
        <InputGroup className="justify-content-center">
            <InputGroupAddon addonType="prepend"><Button onClick={subtract}><i className="fa fa-minus"></i></Button></InputGroupAddon>
            <Input style={{maxWidth: '80px'}} className="text-center" value={itemCount} onChange={(event) => setItemCount(+event.target.value)} />
            <InputGroupAddon addonType="append"><Button onClick={add}><i className="fa fa-plus"></i></Button></InputGroupAddon>
        </InputGroup>
    )
}

export default QuantityInput;