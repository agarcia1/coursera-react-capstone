import { imageTypes } from './imageTypes';

const artworkImages = [
    {
        rowId: 0,
        id: 0,
        source: 'images/pexels-steve-johnson-1266808.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 0,
        id: 1,
        source: 'images/pexels-steve-johnson-1266808-2.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 0,
        id: 2,
        source: 'images/pexels-steve-johnson-1843716.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 1,
        id: 3,
        source: 'images/pexels-zaksheuskaya-1561020.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 1,
        id: 4,
        source: 'images/pexels-steve-johnson-1787242.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 1,
        id: 5,
        source: 'images/pexels-free-creative-stuff-1193743.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 2,
        id: 6,
        source: 'images/pexels-steve-johnson-1856455.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 2,
        id: 7,
        source: 'images/pexels-steve-johnson-1856456.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 2,
        id: 8,
        source: 'images/pexels-steve-johnson-1985682.jpg',
        description: imageTypes.abstractPainting
    },
    {
        rowId: 0,
        id: 9,
        source: 'images/pexels-sam-lion-5709641.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 0,
        id: 10,
        source: 'images/pexels-leo-cardelli-1236701.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 0,
        id: 11,
        source: 'images/pexels-luizclas-556665.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 1,
        id: 12,
        source: 'images/pexels-hristo-fidanov-1252869.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 1,
        id: 13,
        source: 'images/pexels-rahul-695644.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 1,
        id: 14,
        source: 'images/pexels-stephan-müller-1473215.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 2,
        id: 15,
        source: 'images/pexels-dreamlens-production-5837666.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 2,
        id: 16,
        source: 'images/pexels-pixabay-36717.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 2,
        id: 17,
        source: 'images/pexels-dziana-hasanbekava-5905257.jpg',
        description: imageTypes.photograph
    },
    {
        rowId: 0,
        id: 18,
        source: 'images/pexels-gabby-k-5063577.jpg',
        description: imageTypes.pottery
    },
    {
        rowId: 0,
        id: 19,
        source: 'images/pexels-sunsetoned-5913195.jpg',
        description: imageTypes.pottery
    },
    {
        rowId: 0,
        id: 20,
        source: 'images/pexels-karolina-grabowska-4202938.jpg',
        description: imageTypes.pottery
    }
];

export default artworkImages;