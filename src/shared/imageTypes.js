export const imageTypes = {
    photograph: 'photograph',
    abstractPainting: 'abstract painting',
    pottery: 'pottery',
    shirt: 'shirt',
    coaster: 'coaster',
    mug: 'mug',
    hoodie: 'hoodie'
};